function test_ewins_gleeson(j, k, fig_dir, subtitle)
%TEST_EWINS_GLEESON Test the function ewins_gleeson
%   This function is just a test case of the ewins_gleeson function.
%
%   j: index of the output node
%   k: index of the input node
%   fig_dir: name of the folder where the figure should be saved
%   subtitle: (optional) any extra words to be added to the figure's name

    % Testing values
    %[w_vec, FRF_mat] = test_model_accel(4, 1, 1, 1e-3, [0 3], 1e5);
    %FRF_vec = FRF_mat{1, 1};
    %w_res = [0 .7654 1.414 1.848];
    %w_ch = [1e-3 .6 1.3 1.8];
    
    % Real application values
    FRF_vec = read_FRF_file(sprintf('H%d%d.DAT', j, k));
    w_vec = linspace(0, 625, 984);
    w_res = [0 24.8 64.85 130.3 215.5 322.4 448.9 596.4];
    w_ch = [.7 20.98 40.06 59.77 90.92 122.7 162.8 206 258.8 310.3 373.2 435.5 508.6 581.1];
    %w_ch = [.7 17.17 25.43 54.04 65.49 114.4 131 195.8 216.2 297.6 323 421.5 449.5 565.9 597];
    %w_ch = [.7 25.43 65.49 131 216.2 323 449.5 597];
    
    % Compare the original and the identified FRFs for each configuration
    % of sensor and actuator.
    % Do not forget to pass the frequencies in rad/s
    [A_jk, eta_jk] = ewins_gleeson(w_vec*2*pi, FRF_vec, w_res*2*pi, w_ch*2*pi, 1e-8);

    figure('Position', [5, 50, 700, 200], ...
        'PaperUnits', 'centimeters', 'PaperSize', [12 6]);
    semilogy(w_vec, abs(FRF_vec), ...
        w_vec, abs(ident_FRF(w_vec, A_jk, w_res, eta_jk)));
    grid on
    xlabel('\omega')
    ylabel('Magnitude')
    title('Comparacao entre as FRFs original e identificada')
    print('-dpng','-r300', sprintf('%sEG_H%d%dmag_%s', fig_dir, j, k, subtitle))

    figure('Position', [5, 350, 700, 200], ...
        'PaperUnits', 'centimeters', 'PaperSize', [12 6]);
    plot(w_vec, unwrap(angle(FRF_vec)), ...
        w_vec, unwrap(angle(ident_FRF(w_vec, A_jk, w_res, eta_jk))));
    xlabel('\omega')
    ylabel('\Phi')
    grid on
    title('Comparacao entre as FRFs original e identificada')
    print('-dpng','-r300', sprintf('%sEG_H%d%dphs_%s', fig_dir, j, k, subtitle))

    figure('Position', [720, 50, 600, 600], ...
        'PaperUnits', 'centimeters', 'PaperSize', [12 6]);
    radius_exp = log(abs(FRF_vec));
    angle_exp = unwrap(angle(FRF_vec));
    radius_idf = log(abs(ident_FRF(w_vec, A_jk, w_res, eta_jk)));
    angle_idf = unwrap(angle(ident_FRF(w_vec, A_jk, w_res, eta_jk)));
    plot3(w_vec, radius_exp.*cos(angle_exp), radius_exp.*sin(angle_exp), ...
        w_vec, radius_idf.*cos(angle_idf), radius_idf.*sin(angle_idf));
    title('Comparacao entre as FRFs original e identificada, 3D')
    print('-dpng','-r300', sprintf('%sEG_H%d%d_3D_%s', fig_dir, j, k, subtitle))
    
    fprintf('Ajk:\n')
    disp(A_jk)
    fprintf('\neta:\n')
    disp(eta_jk')
    
    fid = fopen(sprintf('%sEG_H%d%d_%s.txt', fig_dir, j, k, subtitle), 'w');
    fprintf(fid, 'w_res:\n');
    fprintf(fid, '%e\n', w_res);
    fprintf(fid, '\nw_ch:\n');
    fprintf(fid, '%e\n', w_ch);
    fprintf(fid, '\nA_jk:\n');
    fprintf(fid, '%e%+ei\n', real(A_jk), imag(A_jk));
    fprintf(fid, '\neta_jk:\n');
    fprintf(fid, '%e\n', eta_jk);
    fclose(fid);
end

function Y = ident_FRF(w, A_jk, w_res, eta_jk)
    num_samples = length(w);
    num_gdl = length(w_res);
    
    Y = zeros(1, num_samples);
    for sample_i = 1:num_samples
        Y(sample_i) = 0;
        for gdl_i = 1:num_gdl
            Y(sample_i) = Y(sample_i) ...
                -A_jk(gdl_i)*w(sample_i)^2/(w_res(gdl_i)^2-w(sample_i)^2+1i*eta_jk(gdl_i)*w_res(gdl_i)^2);
        end
    end
end