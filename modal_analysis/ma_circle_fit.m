function cfdata = ma_circle_fit(w_points, nyquist_points, w_1, pt_1)
%MA_CIRCLE_FIT Identifies the modal parameters from a SDOF FRF.
%   This function fits a circle to the given FRF points by using the Taubin
% method. The identified parameters are given assuming a single degree of
% freedom (SDOF) FRF.
%
% w_points. A column or row vector containing the frequency values where
% the FRF is measures.
% frf_points. A n-by-2 matrix. The first column contains the real part of
% the FRF points and the second column, the imaginary part. This should be
% the receptance FRF.
    
    tol = 1e-8;
    
    [x_c, y_c, r] = circfit(nyquist_points(:, 1), nyquist_points(:, 2));
    %x_c = par(1);
    %y_c = par(2);
    %r = par(3);
    
    % Plot the FRF data and the fitted circle.
    theta = -pi:(pi/100):pi;
    x_circle = x_c + r*cos(theta);
    y_circle = y_c + r*sin(theta);
    
    circlefit_h = figure('Position', [420, 50, 300, 300],...
        'PaperUnits', 'centimeters', 'PaperSize', [6 6]);
    plot(nyquist_points(:, 1), nyquist_points(:, 2), 'k*',...
        x_circle, y_circle)
    axis equal
    title('Pontos selecionados e circulo ajustado')
    
    % Construct the vector of angles from the given FRF points and the
    % identified circle.
    % Central point.
    pt_c = [x_c, y_c];
    
    % Iterate through the points and calculate the angles.
    num_points = length(nyquist_points);
    theta_vec = zeros(num_points, 1);
    
    figure(circlefit_h)
    hold on
    
    % Calculate the point of maximum rate of angle change.
    theta_0 = atan2(pt_1(2)-y_c, pt_1(1)-x_c);
    for theta_i = 1:num_points
        pt_i = nyquist_points(theta_i, :);
        vec1 = pt_1 - pt_c;
        vec2 = pt_i - pt_c;
        vec3 = pt_i - pt_1;
        a = norm(vec1);
        b = norm(vec2);
        c = norm(vec3);
        theta_abs = acos((a^2+b^2-c^2)/(2*a*b));
        sigchck = cross([vec1 0], [vec2 0]);
        if sigchck(3) < -tol
            theta_abs = -theta_abs;
        elseif sigchck(3) > tol
        else
            theta_abs = 0;
        end
        theta_vec(theta_i) = theta_abs+theta_0;
        x_i = x_c+r*cos(theta_vec(theta_i));
        y_i = y_c+r*sin(theta_vec(theta_i));
        plot([x_c x_i], [y_c, y_i])
    end
    hold off
    
    % Use diff and interpolate the point of maximum.
    w_interp = w_points(1):(w_points(end)-w_points(1))/1e3:w_points(end);
    theta_interp = spline(w_points, theta_vec, w_interp);
    
    theta_h = figure('Position', [420, 400, 300, 300],...
        'PaperUnits', 'centimeters', 'PaperSize', [6 6]);
    subplot(2, 1, 1)
    plot(w_interp, theta_interp);
    xlabel('Frequencia')
    ylabel('Theta, interpolado')
    delta_theta = diff(theta_interp);
    delta_w = diff(w_interp);
    dthetadw = delta_theta./delta_w;
    w_interp2 = (w_interp(1:(end-1))+w_interp(2:end))/2;
    theta_interp2 = (theta_interp(1:(end-1))+theta_interp(2:end))/2;
    subplot(2, 1, 2)
    plot(w_interp2, dthetadw);
    xlabel('frequencia')
    ylabel('d Theta, interpolado')

    [max_w_rate, index] = min(dthetadw);
    
    w_n = w_interp2(index);
    theta_n = theta_interp2(index);
    fprintf('Res freq: %e\nRes Theta: %e\n', ...
        w_n, theta_n);
    
    % The given point corresponds to the resonance frequency.
    pt_c = [x_c, y_c];
    pt_n = pt_c + r*[cos(theta_n) sin(theta_n)];
    figure(circlefit_h)
    hold on
    plot([pt_c(1) pt_n(1)], [pt_c(2) pt_n(2)], 'r-')
    plot(pt_n(1), pt_n(2), 'ko')
    hold off
    
    % Estimate the damping.
    w_x = spline(theta_vec, w_points, [theta_n+pi/2, theta_n-pi/2]);
    eta_id = (w_x(2)^2-w_x(1)^2)/(2*w_n^2);
    fprintf('identified eta = %e\n', eta_id)
    % From the experimental data
     [w_a, w_b] = split_vector(w_points, w_n, tol);
     eta_vec = [];
     for w_ai = w_a
         for w_bi = w_b
             phi_x = abs(spline(w_points, theta_vec, [w_ai w_bi]) - theta_n);
             eta_vec = [eta_vec, (w_bi^2-w_ai^2)/(w_n^2*(tan(phi_x(1)/2)+tan(phi_x(2)/2)))];
         end
     end
     eta_mean = mean(eta_vec);
     eta_std = std(eta_vec);
     fprintf('eta:\nmean = %e\tstd = %e\n', eta_mean, eta_std);

    % Calculate the modal constant
    Ajk_mag = 2*r*eta_id;
    Ajk_arg = theta_n;
    [Ajk_r, Ajk_i] = pol2cart(Ajk_arg-pi/2, Ajk_mag);
    Ajk = Ajk_r + 1i*Ajk_i;
    % Calculate the modal residue and the argument of Ajk
    vec_cn = pt_n - pt_c;
    vec_B = pt_c - vec_cn;
    figure(circlefit_h)
    hold on
    plot(vec_B(1), vec_B(2), 'gd')
    plot([0 vec_B(1) x_c], [0 vec_B(2) y_c], 'g-')
    plot(0, 0, 'k+');
    hold off
    Bjk = vec_B(1)+1i*vec_B(2);
    
    fprintf('Ajk = %e%+ei\n', real(Ajk), imag(Ajk))
    fprintf('Bjk = %e%+ei\n', real(Bjk), imag(Bjk))
    
    cfdata.w_n = w_n;
    cfdata.theta_n = theta_n;
    cfdata.eta_id = eta_id;
    cfdata.eta_mean = eta_mean;
    cfdata.eta_std = eta_std;
    cfdata.Ajk = Ajk;
    cfdata.Bjk = Bjk;
    cfdata.fig_h = [circlefit_h theta_h];
end

function dx = central_diff(x)
    % Linearly extrapolate at the ends
    x_0 = x(1)+x(2)-x(3);
    x_n1 = -x(end-2)+x(end-1)+x(end);
    
    dx = [x(2:end) x_n1]-[x_0 x(1:(end-1))];
end

function [w_a, w_b] = split_vector(w_points, w_n, tol)
    w_a = [];
    w_b = [];
    for w_i = w_points'
        if (w_i - w_n)/w_n < -tol
            w_a = [w_a w_i];
        elseif (w_i - w_n)/w_n > tol
            w_b = [w_b w_i];
        end
    end
end