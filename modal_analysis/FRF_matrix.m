function FRF_mat = FRF_matrix(V, Ddiag, Lambda, B, C, num_modes, num_modes_cor, num_rb_modes)
    % Considerando os num_modes primeiros modos, menos os de corpo r�gido
    ind = (num_rb_modes+1):(num_rb_modes+1+num_modes);
    Phi = V(:, ind);
    Omega2 = Ddiag(ind);

    % Corre��o DC de modos mais altos
    ind_cor = (num_rb_modes+1+num_modes+1):(num_rb_modes+1+num_modes_cor);
    Phi_cor = V(:, ind_cor);
    Omega2_cor = Ddiag(ind_cor);
    
    num_pert = size(B, 2);
    num_sens = size(C, 1);
    
    % Matriz com as FRFs
    FRF_mat = cell(num_sens, num_pert);
    
    % Criar as FRFs e atribui-las � matriz
    for j = 1:num_sens
        for k = 1:num_pert
            FRF_mat{j, k} = @(w) FRF_jk(w, Phi, Omega2, Lambda, B(:, k), C(j, :)) ...
                + dc_correction(Phi_cor, Omega2_cor, B(:, k), C(j, :));
        end
    end
end