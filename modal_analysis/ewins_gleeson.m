function [Ajk, eta] = ewins_gleeson(w_vec, frf_vec, w_res, w_ch, tol)
    % This function implements the simple Ewins-Gleeson method for lightly
    % damped structures. frf_vec is a row vector that contains the value of
    % the FRF at the sampled frequencies in the row vector w_vec.
    % w_res is a row vector that contains the
    % resonance frequencies of the structure. This function returns the
    % modal constants and the modal damping coefficients in the row vectors
    % Ajk and eta, respectively.
    
    eta_small = 1e-8;
    num_res = length(w_res);
    num_ch = length(w_ch);
    %num_samples = length(w_res);
    num_chosen = length(w_ch);
    if abs(w_ch(1)) < tol
        w_ch(1) = tol;
    end
    % Create the matrix of fitting FRFs
    Mat = zeros(num_chosen, num_res);
    for j = 1:num_ch
        for k = 1:num_res
            Mat(j, k) = -w_ch(j)^2/(w_res(k)^2-w_ch(j)^2+1i*eta_small*w_res(k)^2);
        end
    end
    AtA = Mat'*Mat;
    B = interp1(w_vec, frf_vec, w_ch);
    AtB = Mat'*B.';
    Ajk = AtA\AtB;
    
    eta = zeros(1, num_res);
    for j = 1:num_res
        frf_res = interp1(w_vec, frf_vec, w_res(j));
        if abs(w_res(j)) < tol
            eta(j) = tol;
        else
            eta(j) = abs(Ajk(j))/(abs(frf_res)*w_res(j)^2);
        end
    end
end