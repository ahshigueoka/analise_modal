function Y = Accelerance_jk_v(w, Phi, Omega2, Lambda, b, c)
% Returns the FRF of the system represented by the eigenvectors in the
% column of Phi and its associated eigenvalues in the vector Omega2
% containing the square of the natural frequencues. The vector Lambda
% contains the terms for modal damping. b is a column vector
% of inputs (actuators) and c is a row vector of outputs (sensors).
    Y = 0;
    num_modes = size(Phi, 2);
    num_samples = size(w, 2);
    
    for j = 1:num_modes
        for k = 1:num_samples
            Y = Y + (c*Phi(:, j))*(Phi(:, j)'*b) ...
            - w(k)^2/(Omega2(j)-w(k)^2+1i*w(k)*Lambda(j));
        end
    end
end