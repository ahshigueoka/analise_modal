function FRF_cor = dc_correction(Phi, Omega2_cor, b, c)
    FRF_cor = 0;
    num_modes = size(Phi, 2);
    
    for m = 1:num_modes
        FRF_cor = FRF_cor + (c*Phi(:, m))*(Phi(:, m)'*b)/Omega2_cor(m);
    end
end