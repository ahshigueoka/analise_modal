function [w_vec, FRF_mat] = test_model(num_gdl, m, k, c, w_span, num_samples)
% This function returns the discrete FRF matrix where the row index
% represents the position of the sensor, the column index represents the
% position of the actuator and the third dimension represents the frequency
% component of the FRF. So each Hjk(w) extends through the third index,
% that is, given j and k, each value for l represents one frequency of the
% FRF Hjk(wl) = frf_mat(j, k, l)

    M = eye(num_gdl)*m;
    if num_gdl == 1
        K = k;
    elseif num_gdl > 1
        K = toeplitz([2*k -k zeros(1, num_gdl-2)]);
    else
        error('Negative value for num_gdl')
    end
        
    Lambda = ones(1, num_gdl)*c;
    [S, D] = eig(K, M);
    Omega2 = diag(D);
    w_vec = linspace(w_span(1), w_span(2), num_samples);
    % Create the FRF matrix
    FRF_mat = cell(num_gdl, num_gdl);
    for j = 1:num_gdl
        c = zeros(1, num_gdl);
        c(j) = 1;
        for k = 1:num_gdl
            b = zeros(num_gdl, 1);
            b(k) = 1;
            FRF_mat{j, k} = FRF_jk(w_vec, S, Omega2, Lambda, b, c);
        end
    end
end