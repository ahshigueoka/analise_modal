function wph_vec = find_phase(w_vec, FRF_comp, ph)
%FIND_PHASE Returns all the points in the given FRF with ph phase
%
% This function looks for values of w_vec where the phase of the FRF
% changes from a value less than ph to a value grater than ph. If such an
% interval is found, then the value corresponding to phase ph is estimated
% from interpolation. If the value the extreme of the interval
    FRF_angle = angle(FRF_comp) - ph;
    
    wph_vec = fmultzero_vec(w_vec, FRF_angle);
end

