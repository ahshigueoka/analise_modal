function M = revert_col(A)
   n = size(A, 2);
   E = zeros(n);
   
   for j = 1:n
       E(j, n+1-j) = 1;
   end
   
   M = A*E;
end