function solutions = fmultzero_vec(x, w)

x_len = length(x);

%Vetor de intervalos
interval = zeros(x_len, 2);

y_ii = w(1);
num_intervals = 0;
for ind = 2:x_len
    y_i = y_ii;
    y_ii = w(ind);
    
    if y_i*y_ii < 0
        num_intervals = num_intervals + 1;
        interval(num_intervals, :) = [ind-1 ind];
    end
end

solutions = zeros(1, num_intervals);
for i_sol = 1:num_intervals
    ind = interval(i_sol, :);
    solutions(i_sol) = x(ind(1))-w(ind(1))*(x(ind(2))-x(ind(1)))/(w(ind(2))-w(ind(1)));
end
