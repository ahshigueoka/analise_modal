function geo = geometry()
    % Geometria do modelo
    geo.l1 = 19.93e-2; % 19.93 cm
    geo.l2 = 19.93e-2; % 19.93 cm
    geo.l3 = 8.2e-2; %8.2 cm
    geo.l4 = 10.31e-2; % 10.31 cm
    geo.l5 = 40.0e-2; % 40.0 cm
    geo.l6 = 2e-2; % 2cm
    geo.l7 = 4e-2; % 4cm
    geo.b_viga = 2.7e-2; % 2.7 cm
    geo.h_viga = 0.2e-2; % 0.2 cm
    geo.L_m = 2.82e-2; % 2.82 cm
    geo.L_n = 3.16e-2; % 3.16 cm
    
    %geo.l1 = 100; % 19.93 cm
    %geo.l2 = 100; % 19.93 cm
    %geo.l3 = 50; %8.2 cm
    %geo.b_viga = 1; % 2.7 cm
    %geo.h_viga = 1; % 0.2 cm
    %geo.L_m = 1; % 2.82 cm
end
