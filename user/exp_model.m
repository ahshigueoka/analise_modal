function FRF_mat = exp_model()
    n = 8;
    
    FRF_mat = cell(n, n);
    
    for j = 1:n
        for k = j:n
            filename = sprintf('H%d%d.DAT', j, k);
            FRF_mat{j, k} = read_FRF_file(filename);
            %frf_vec = read_FRF_file(filename);
            %FRF_mat{j, k} = frf_vec(2:end);
            if j ~= k
                FRF_mat{k, j} = conj(FRF_mat{j, k});
            end
        end
    end
end