function y = separate(V, nodes_gdl, gdl_i, c)
    v = V(:, c);
    n = length(v);
    y = zeros(1, n/nodes_gdl);
    for i = 1:n/nodes_gdl
        y(i) = v(nodes_gdl*(i-1)+gdl_i);
    end
end