function FRF_vec = read_FRF_file(filename)
% READ_FRF_FILE Read the FRF file created by the instruments and returns an
% FRF row vector that is appropriate for operations in MATLAB, such as
% plotting.
    fid = fopen(filename, 'r', 'l', 'windows-1252');
    
    % Read the format of three columns of complex numbers
    FRF_coef = textscan(fid, '%f %f');
    
    % Add the real and the complex parts
    FRF_vec = (FRF_coef{1} + 1i*FRF_coef{2}).';
    
    fclose(fid);
end