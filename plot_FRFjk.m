function plot_FRFjk(j, k, opt, fig_dir)
% Plots the experimental FRF
%
% This function loads the experimental FRF data and plots in a magnitude
% and phase format.
%
% j: the index of the output node
% k: the index of the input node
%
% opt: a struct containing options for the plot function.
% opt.x:
%     if opt.x == 0, this plot will show the data indexes in the X axis.
%     if opt.x == 1, this plot will show the 0-625Hz frequency range in 
%         the X axis.
% opt.print:
%     if opt.print == 0, this plot will not be saved.
%     if opt.print == 1, this plot will be saved in the fig_dir directory.
%         note that magnitude and phase will be saved in separate files
%             FRFjkmag.png and FRFjkphs.png
%
% fig_dir:    the directory where the files are supposed to be saved.
    if opt.x == 0
        w_vec = 1:984;
    elseif opt.x == 1
        w_vec = linspace(0, 625, 984);
    else
        error('There is no such option for opt.\n')
    end
    
    if j > k
        FRF_vec = conj(read_FRF_file(sprintf('H%d%d.DAT', k, j)));
    else
        FRF_vec = read_FRF_file(sprintf('H%d%d.DAT', j, k));
    end
    
    figure('Position', [5, 50, 800, 200], ...
        'PaperUnits', 'centimeters', 'PaperSize', [12 3]);
    semilogy(w_vec, abs(FRF_vec));
    xlabel('\omega')
    ylabel(sprintf('|H_{%d%d}|', j, k))
    if opt.print == 1
        print('-dpng','-r300', sprintf('%sFRF%d%dmag', fig_dir, j, k))
    end
    
    figure('Position', [5, 350, 800, 200], ...
        'PaperUnits', 'centimeters', 'PaperSize', [12 3]);
    plot(w_vec, unwrap(angle(FRF_vec)));
    xlabel('\omega')
    ylabel(sprintf('Fase H_{%d%d}', j, k))
    grid on
    
    if opt.print == 1
        print('-dpng','-r300', sprintf('%sFRF%d%dphs', fig_dir, j, k))
    end
end