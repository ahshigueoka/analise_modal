function y = hermite_1(eps, l)
    y = 1-3*eps.^2+2*eps.^3;
end