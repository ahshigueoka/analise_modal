function y = hermite_2(eps, l)
    y = l*eps.*(1-eps).^2;
end