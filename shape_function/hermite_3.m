function y = hermite_3(eps, l)
    y = eps.^2.*(3-2*eps);
end