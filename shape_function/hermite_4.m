function y = hermite_4(eps, l)
    y = l*eps.^2.*(eps-1);
end