function test_circle_fit(j_ind, k_ind, w_index, w_vec, fig_dir, subtitle)
    %[w_vec, FRF_mat] = test_model(3, 1, 1, .01, [0 3], 1e3);
    %FRF = FRF_mat{1, 1};
    
    FRF = read_FRF_file(sprintf('H%d%d.DAT', j_ind, k_ind));
    FRF = FRF(w_index);
    w_vec = w_vec(w_index);
    
    FRF_h = figure('Position', [10, 50, 400, 200], ...
        'PaperUnits', 'centimeters', 'PaperSize', [12 6]);
    title('FRFs experimental e identificada')
    semilogy(w_vec, abs(FRF), 'b-')
    
    query_h = figure('Position', [730, 50, 600, 600], ...
        'PaperUnits', 'centimeters', 'PaperSize', [12 12]);
    plot3(w_vec, real(FRF), imag(FRF), 'b+-');
    axis equal
    view([90, 0]);
    title('Escolha os pontos. Enter para concluir');
    dcm_obj = datacursormode(query_h);
    set(dcm_obj, 'UpdateFcn', @mod_updatefcn,...
        'DisplayStyle','datatip', 'SnapToDataVertex','on','Enable','on')
    plot_data = [];
    disp('Clique nos pontos para selecionar. Enter para concluir.')
    pause
    
    ylabel('real(A(\omega))')
    zlabel('imag(A(\omega))')
    
    clean_data = remove_repeated(plot_data, 1e-8);
    
%     sel_pts_h = figure('Units', 'centimeters', 'Position', [8, 2, 7, 7]);
%     hold on
%     plot(real(FRF), imag(FRF), 'b+-')
%     plot(clean_data(:, 2), clean_data(:, 3), 'ms')
%     hold off
%     axis equal
%     title('Pontos selecionados')
%     xlabel('real(A(\omega))')
%     ylabel('imag(A(\omega))')
    
    w_ref = clean_data(1, 1);
    pt_ref = clean_data(1, 2:3);

    w_n = clean_data(:, 1);
    points_n = clean_data(:, 2:3);
    
    [w_n, IX] = sort(w_n);
    points_n = points_n(IX, :);
    
    cfdata = ma_circle_fit(w_n, points_n, w_ref, pt_ref);
    w_n = cfdata.w_n;
    eta_id = cfdata.eta_id;
    Ajk = cfdata.Ajk;
    Bjk = cfdata.Bjk;
    
    figure(FRF_h)
    y = Ajk./(1-w_n^2./w_vec.^2*(1+1i*eta_id))+Bjk;
    hold on
    semilogy(w_vec, abs(y), 'r--')
    hold off
    legend('FRF experimental', 'FRF SDOF circle fit')
    title('Comparacao entre FRFs experimental e modificada')
    xlabel('\omega')
    ylabel('real(A(\omega))')
    zlabel('imag(A(\omega))')
    
    FRF3D_h = figure('Position', [10, 300, 400, 400],...
        'PaperUnits', 'centimeters', 'PaperSize', [6 6]);
        plot3(w_vec, real(FRF), imag(FRF), 'b-',...
        w_vec, real(y), imag(y), 'r--');
    grid on
    title('Comparacao entre FRFs experimental e identificada')
    xlabel('\omega')
    ylabel('A(\omega)')
    
    figure(FRF_h)
    print('-dpng','-r300', sprintf('%sCF_Comp_Mag_FRF%d%d_%s', fig_dir, j_ind, k_ind, subtitle))
    figure(query_h)
    print('-dpng','-r300', sprintf('%sCF_SelPoints_FRF%d%d_%s', fig_dir, j_ind, k_ind, subtitle))
    figure(cfdata.fig_h(1))
    print('-dpng','-r300', sprintf('%sCF_Circle_FRF%d%d_%s', fig_dir, j_ind, k_ind, subtitle))
    figure(cfdata.fig_h(2))
    print('-dpng','-r300', sprintf('%sCF_Theta_FRF%d%d_%s', fig_dir, j_ind, k_ind, subtitle))
    figure(FRF3D_h)
    print('-dpng','-r300', sprintf('%sCF_Comp_3D_FRF%d%d_%s', fig_dir, j_ind, k_ind, subtitle))
    
    fid = fopen(sprintf('%sCF_H%d%d_%s.txt', fig_dir, j_ind, k_ind, subtitle), 'w');
    fprintf(fid, 'Faixa de indices: %d:%d\n', w_index(1), w_index(end));
    fprintf(fid, 'Faixa de frequencias: [%e %e]\n', w_vec(1), w_vec(2));
    fprintf(fid, 'Ajk: %e%+ei\n', real(Ajk), imag(Ajk));
    fprintf(fid, 'Bjk: %e%+ei\n', real(Bjk), imag(Bjk));
    fprintf(fid, 'w_n: %e\n', w_n);
    fprintf(fid, 'theta_n: %e\n', cfdata.theta_n);
    fprintf(fid, 'eta_n:\n   eta_id: %e\n', eta_id);
    fprintf(fid, '   mean(eta): %e\n', cfdata.eta_mean);
    fprintf(fid, '   std(eta) : %e\n', cfdata.eta_std);
    fclose(fid);
    
    function txt = mod_updatefcn(empt,event_obj)
        pos = get(event_obj, 'Position');
        
        hold on
        plot3(pos(1), pos(2), pos(3), 'ms');
        hold off
        
        %txt = {['freq:' , num2str(pos(1))],...
        %       ['Real: ', num2str(pos(2))],...
        %       ['Imag: ', num2str(pos(3))]};
        txt = {''};
        plot_data = [plot_data; pos];
    end
end

function clean_data = remove_repeated(pt_data, tol)
    num_pts = size(pt_data, 1);
    
    clean_data = zeros(size(pt_data));
    
    num_dif_pts = 1;
    clean_data(1, :) = pt_data(1, :);
    for pt_i = 2:num_pts
        flag = true;
        for num_i = 1:num_dif_pts
            if norm(pt_data(pt_i) - clean_data(num_i)) < tol
                flag = false;
            end
        end
        
        if flag
            num_dif_pts = num_dif_pts + 1;
            clean_data(num_dif_pts, :) = pt_data(pt_i, :);
        end
    end
    
    clean_data = clean_data(1:num_dif_pts, :);
end