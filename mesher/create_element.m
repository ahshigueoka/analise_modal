function mesh = create_element(mesh, grid, type, nodes, geo, prop)
    mesh.count = mesh.count + 1;
    
    create_func = str2func(['create_' type]);
    el = create_func(grid, nodes, geo, prop);
    el.field_func = str2func([type '_field']);
    
    mesh.el_list = cat(1, mesh.el_list, el);
end