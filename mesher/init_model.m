function [grid, mesh] = init_model(el_type)
    
    switch el_type
        case 'beam_eb'
            num_gdl = 2;
        case 'port_eb'
            num_gdl = 3;
        otherwise
            error('Unsupported type of element: %s\n', el_type);
    end
    grid.count = 0;
    grid.node_list = [];
    grid.node_gdl = num_gdl;

    mesh.count = 0;
    mesh.el_list = [];
end