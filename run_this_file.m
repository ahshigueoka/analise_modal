function run_this_file()
    j = 1;
    k = 1;
    fig_dir = 'figures/';

    %test_ewins_gleeson(j, k, fig_dir, 'def');

    close all

    w_index = 28:87;
    w_vec = linspace(0, 625, 984);
    test_circle_fit(j, k, w_index, w_vec, fig_dir, 'res05')
end