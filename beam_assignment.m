clear all
close all
clc

dir_figuras = 'figures\';

%% Carregar propriedades
geo.l_viga = 160e-3; % 92 pol
geo.b_viga = 20e-3; % 1 1/2 pol
geo.h_viga = 1e-3; % 1 pol
geo.a_viga = geo.b_viga*geo.h_viga; % area da viga

prop.mu = 7.80e3*geo.a_viga; % Densidade linear da viga
prop.E = 200e9; % 200 GPa
prop.I = geo.b_viga*geo.h_viga^3/12;

[grid, mesh] = init_model('beam_eb');

ELEMENT_SIZE = 1e-2;

%% Criar a malha de elementos

% Calcular o n�mero de n�s e elementos necess�rios para que a malha
% tenha elementos com tamanho igual ou menor a ELEMENT_SIZE.
% num_el = ceil(geo.l_viga/ELEMENT_SIZE);
% num_nodes = num_el + 1;
% Inserir um n�mero fixo de n�s e elementos
num_el = 300;
num_nodes = 301;

% Pontos horizontais igualmente espa�ados ao longo da viga.
points_x = linspace(0, geo.l_viga, num_nodes);
points_y = zeros(size(points_x));

% Inserir os n�s na grade a partir dos pontos criados.
for node_i = 1:num_nodes
    grid = create_node(grid, [points_x(node_i), points_y(node_i)]);
end

% Criar os elementos a partir dos n�s de grade.
for el_i = 1:num_el
    el_nodes = [el_i, el_i+1];
    mesh = create_element(mesh, grid, 'beam_eb', el_nodes, geo, prop);
end

%% Criar a matrix de rigidez global a partir do modelo.
Kg = assemble_mesh(mesh, grid, 'k');

%% Criar a matriz de in�rcia global a partir do modelo.
Mg = assemble_mesh(mesh, grid, 'm');

m1 = 2*.96*0.948*4.5*2.70e-3;
m2 = (1.264*8 + 1.257*8 - (1.257 - 0.751) * 7)*2.53*2.70e-3;
I1 = m1*((18.96e-3)^2 + (9.6e-3)^2)/12;
I2 = m2*((25.21e-3)^2 + (80e-3)^2)/12;
Mg(1, 1) = Mg(1, 1) + m1;
Mg(2, 2) = Mg(2, 2) + I1;
Mg(301, 301) = Mg(301, 301) + m2;
Mg(301, 301) = Mg(301, 301) + I2;
Mg(601, 601) = Mg(601, 601) + m1;
Mg(602, 602) = Mg(602, 602) + I1;

%% Matrix de amortecimento global
% Supondo amortecimento modal constante de 1%. Substituir pelo valor
% experimental mais tarde.
Lambda = 1e-5*ones(1, size(Kg, 1));

%% Calcular autovalores e autovetores
num_gdl = grid.count*grid.node_gdl;
num_modes = 10;
num_modes_cor = 20;
num_rb_modes = 2;
[V, D] = eig(Kg, Mg, 'qz', 'vector');
[Omega2, idx] = sort(D);
V = V(:, idx);

x_n = grid.node_list(:, 1);

% Extrair as componentes de deflex�o do modelo
% dim   GDL
%   1     u
%   2     v
%   3     theta
dim = 1;

figure('Position', [50 50 400 400], 'PaperUnits', 'centimeters', 'PaperSize', [6 6])
% Plotar os autovetores
for mode_i = 1:num_modes
    v_n = separate(V, grid.node_gdl, dim, mode_i);
    plot(x_n, v_n)
    title(sprintf('Viga modo %d: w=%e', mode_i, sqrt(Omega2(mode_i))/(2*pi)));
    print('-dpng','-r300', sprintf('%sViga_Modo_%d', dir_figuras, mode_i))
end

%% C�lculo das FRFs

% N�mero de componentes dos autovetores = n�mero de n�s no modelo
% N�mero de configura��es diferentes de perturba��o.
num_pert = 4;
% N�mero de configura��es diferentes de sensores.
num_sens = 4;
% Pontos em que a perturba��o foi aplicada.
B = zeros(num_gdl, num_pert);
B(  1, 1) = 1;
B(101, 2) = 1;
B(201, 3) = 1;
B(301, 4) = 1;
% Pontos dos sensores.
C = zeros(num_sens, num_gdl);
C(1,  1) = 1;
C(2, 101) = 1;
C(3, 201) = 1;
C(4, 301) = 1;

% C�lculo da matriz de FRFs.
FRF_mat = FRF_matrix(V, Omega2, Lambda, B, C, num_modes, num_modes_cor, num_rb_modes);

w_vec = linspace(0, 625, 984);
FRF_fun = FRF_mat{1, 1};
FRF_vec = FRF_fun(w_vec*2*pi);
% Converter para aceler�ncia
FRF_vec = FRF_vec.*(-(w_vec*2*pi).^2);

figure('Position', [50, 50, 800, 200], ...
    'PaperUnits', 'centimeters', 'Papersize', [12 3])
semilogy(w_vec, abs(FRF_vec));
xlabel('\omega')
ylabel('|H(\omega)|')
title('FRF da viga livre-livre');
print('-dpng', '-r300', sprintf('%sViga_H11_mag', dir_figuras))

figure('Position', [50, 350, 800, 200], ...
    'PaperUnits', 'centimeters', 'Papersize', [12 3])
plot(w_vec, angle(FRF_vec));
xlabel('\omega')
ylabel('Fase H(\omega)')
title('FRF da viga livre-livre');
print('-dpng', '-r300', sprintf('%sViga_H11_phs', dir_figuras))

fh = figure('Position', [850, 50, 400, 400], ...
    'PaperUnits', 'centimeters', 'Papersize', [6 6]);
plot_field(grid, mesh, V(:, 3)', 10, fh)