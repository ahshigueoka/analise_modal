function plot_all_exp_FRF()
    blacklist = [4, 4;
                 6, 8;
                 8, 6];
    
    opt.x = 1;
    opt.print = 0;
    for j = 1:8
        for k = j:8
            close all
            if correct_FRF(j, k, blacklist)
                plot_FRFjk(j, k, opt, 'figures/');
            end
        end
    end
end

function flag = correct_FRF(j, k, blacklist)
    flag = true;
    blacklist_len = size(blacklist, 1);
    
    for item_i = 1:blacklist_len
        if isequal([j, k], blacklist(item_i, :))
            flag = false;
            break
        end
    end
end