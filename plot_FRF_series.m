function plot_FRF_series(k)
    close all
    w_vec = linspace(0, 625, 984);
    FRF_series = zeros(8, 984);
    colors = [ 1  0  0;...
              .9  0 .1;...
              .8  0 .2;...
              .7  0 .3;...
              .6  0 .4;...
              .5  0 .5;...
              .4  0 .6;...
              .3  0 .7];
    
    
    figure('PaperSize', [6, 6])
    hold on
    for j = 1:8
        if j < k
            FRF_series(k, :) = read_FRF_file(sprintf('H%d%d.DAT', j, k));
        else
            FRF_series(k, :) = conj(read_FRF_file(sprintf('H%d%d.DAT', k, j)));
        end
        plot3(w_vec, j*ones(size(w_vec)), log(abs(FRF_series(k, :))), 'Color', colors(j, :))
    end
    hold off
end