function plot_field(grid, mesh, nodal_param_g, lod, fh)
    % Find all the nodes in each element, pass the nodal parameters and
    % reconstruct that element's field. Then plot it.
    num_el = mesh.count;
    
    figure(fh)
    clf
    hold on
    for el_i = 1:num_el
        el = mesh.el_list(el_i);
        % Allocate space for the points in the element.
        % Each row represents one node point in the element
        num_nodes = size(el.nodes, 2);
        % Each column, the components of the points in X, Y and Z
        num_comp = size(grid.node_list, 2);
        % Preallocate space for the node points in the elements
        points = zeros(num_nodes, num_comp);
        nodal_params = zeros(num_nodes, grid.node_gdl);
        for j = 1:num_nodes
            points(j, :) = grid.node_list(el.nodes(j), :);
            nodal_params(j, :) = nodal_param_g((grid.node_gdl*(el.nodes(j)-1)+1):(grid.node_gdl*el.nodes(j)));
        end
        % Get the interpolated field
        [x, y] = el.field_func(points, nodal_params, el, lod);
        % Plot the field
        plot(x, y, '-k');
    end
    hold off
end