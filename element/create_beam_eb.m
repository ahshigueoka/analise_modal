function el = create_beam_eb(grid, nodes, geo, prop)
    el.type = 'beam_eb';
    el.nodes = nodes;
    el.geo = geo;
    n1 = nodes(1);
    n2 = nodes(2);
    p1 = grid.node_list(n1, :);
    p2 = grid.node_list(n2, :);
    el.geo.l = norm(p1 - p2, 2);
    el.prop = prop;
end