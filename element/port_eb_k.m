function Ke = port_eb_k(el)
    l = el.geo.l;
    Ke = [0,   0,     0,   0,    0,     0;
          0,  12,   6*l,   0,  -12,   6*l;
          0, 6*l, 4*l^2,   0, -6*l, 2*l^2;
          0,   0,     0,   0,    0,     0;
          0, -12,  -6*l,   0,   12,  -6*l;
          0, 6*l, 2*l^2,   0, -6*l, 4*l^2];
      
    Ke = Ke*el.prop.E*el.prop.I/l^3;
    Ke(1, 1) =  el.prop.E*el.geo.A/l;
    Ke(4, 4) =  el.prop.E*el.geo.A/l;
    Ke(1, 4) = -el.prop.E*el.geo.A/l;
    Ke(4, 1) = -el.prop.E*el.geo.A/l;
    t = el.geo.theta;
    R = [ cos(t), sin(t), 0;
         -sin(t), cos(t), 0;
              0,     0,   1];
    T = [R, zeros(3);
         zeros(3), R];
    % Transformação para o sistema de coordenadas global
    Ke = T'*Ke*T;
end