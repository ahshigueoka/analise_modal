function Ke = beam_eb_k(el)
    l = el.geo.l;
    Ke = [   12,   6*l,   -12,   6*l;
            6*l, 4*l^2,  -6*l, 2*l^2;
            -12,  -6*l,    12,  -6*l;
            6*l, 2*l^2,  -6*l, 4*l^2];
    Ke = Ke*el.prop.E*el.prop.I/l^3;
end