function [x, y] = port_eb_field(points, nodal_params, el, lod)
    eps = linspace(0, 1, lod);
    p1 = points(1, :);
    p2 = points(2, :);
    l = norm(p1 - p2, 2);
    
    t = el.geo.theta;
    R2 = [ cos(t), sin(t);
          -sin(t), cos(t)];
    R3 = [ cos(t), sin(t), 0;
          -sin(t), cos(t), 0;
                0,      0, 1;];
    
    nodal_params = nodal_params*R3';
    
    delta_x = (nodal_params(2, 1)-nodal_params(1, 1))*eps+nodal_params(1, 1);
    delta_y = nodal_params(1, 2:3)*[hermite_1(eps, l); hermite_2(eps, l)] + ...
              nodal_params(2, 2:3)*[hermite_3(eps, l); hermite_4(eps, l)];
          
    % Rotate the displacement field.
    rot_field = R2'*[delta_x; delta_y];
    
    x = rot_field(1, :) + linspace(p1(1), p2(1), lod);
    y = rot_field(2, :) + linspace(p1(2), p2(2), lod);
end