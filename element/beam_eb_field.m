function [x, y] = beam_eb_field(points, nodal_params, el, lod)
    eps = linspace(0, 1, lod);
    p1 = points(1, :);
    p2 = points(2, :);
    l = norm(p1 - p2, 2);
    
    delta_x = linspace(p1(1), p2(1), lod);
    delta_y = nodal_params(1, :)*[hermite_1(eps, l); hermite_2(eps, l)] + ...
              nodal_params(2, :)*[hermite_3(eps, l); hermite_4(eps, l)];
    
    x = delta_x;
    y = delta_y + p1(2);
end