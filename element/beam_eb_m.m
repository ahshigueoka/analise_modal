function Me = beam_eb_m(el)
    l = el.geo.l;
    
    Me = [  156,   22*l,    54,  -13*l;
           22*l,  4*l^2,  13*l, -3*l^2;
             54,   13*l,   156,  -22*l;
          -13*l, -3*l^2, -22*l,  4*l^2];
    Me = Me*el.prop.mu*l/420;
end