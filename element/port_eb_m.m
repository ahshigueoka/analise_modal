function Me = port_eb_m(el)
    l = el.geo.l;
    Me = [0,     0,      0,   0,     0,      0;
          0,   156,   22*l,   0,    54,  -13*l;
          0,  22*l,  4*l^2,   0,  13*l, -3*l^2;
          0,     0,      0,   0,     0,      0;
          0,    54,   13*l,   0,   156,  -22*l;
          0, -13*l, -3*l^2,   0, -22*l,  4*l^2];
    Me = Me*el.prop.mu*l/420;
    Me(1, 1) = el.prop.mu*l/3;
    Me(4, 4) = el.prop.mu*l/3;
    Me(1, 4) = el.prop.mu*l/6;
    Me(4, 1) = el.prop.mu*l/6;
    t = el.geo.theta;
    R = [ cos(t), sin(t), 0;
         -sin(t), cos(t), 0;
              0,     0,   1];
    T = [R, zeros(3);
         zeros(3), R];
    % Transformação para o sistema de coordenadas global
    Me = T'*Me*T;
end