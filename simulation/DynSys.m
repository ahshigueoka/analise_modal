classdef DynSys < handle
    properties
        A;
        B;
        pert;
    end
    
    methods (Access=public)
        function obj = DynSys(A, B, pert)
            obj.A = A;
            obj.B = B;
            obj.pert = pert;
        end
        
        function dydt = ssf(obj, t, y)
            dydt = obj.A*y + obj.B*obj.pert.pert_force(t);
        end
    end
    
end

