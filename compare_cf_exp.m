function compare_cf_exp()
    close all
    j = 1;
    k = 1;
    fig_dir = 'figures/';
    A_jk = [1.319338e-01-4.667356e-03i, ...
        1.117729e-01-2.693191e-03i, ...
        1.076382e-01-8.145635e-03i, ...
        1.012976e-01-2.954320e-03i, ...
        9.828857e-02+5.886078e-03i, ...
        8.764005e-02+7.004050e-03i, ...
        7.593882e-02-4.003629e-03i];
    eta_jk = [7.140740e-02, ...
        3.891234e-02, ...
        1.512825e-02, ...
        9.098394e-03, ...
        5.280006e-03, ...
        4.305852e-03, ...
        3.801497e-03];
    w_res = [2.487602e+01, ...
        6.497266e+01, ...
        1.305481e+02, ...
        2.158227e+02, ...
        3.220870e+02, ...
        4.487551e+02, ...
        5.965177e+02];

    w_vec = linspace(0, 625, 984);
    FRF_vec = read_FRF_file(sprintf('H%d%d.DAT', j, k));
    
    figure('Position', [5, 50, 800, 200], ...
        'PaperUnits', 'centimeters', 'PaperSize', [12 3]);
    semilogy(w_vec, abs(FRF_vec), 'b-');
    
    FRF_cf = ident_FRF(w_vec, A_jk, w_res, eta_jk, .141628);
    
    hold on
    semilogy(w_vec, abs(FRF_cf), 'r-');
    hold off
    
    xlabel('\omega')
    ylabel(sprintf('|H_{%d%d}|', j, k))
    
    print('-dpng','-r300', sprintf('%sCF_cmp_H%d%dmag', fig_dir, j, k))
    
    figure('Position', [5, 350, 800, 200], ...
        'PaperUnits', 'centimeters', 'PaperSize', [12 3]);
    hold on
    plot(w_vec, unwrap(angle(FRF_vec)), 'b-');
    plot(w_vec, unwrap(angle(FRF_cf)), 'r-');
    hold off
    
    xlabel('\omega')
    ylabel(sprintf('Fase H_{%d%d}', j, k))
    
    print('-dpng','-r300', sprintf('%sCF_cmp_H%d%dphs', fig_dir, j, k))
    
    figure('Position', [820, 050, 400, 400], ...
        'PaperUnits', 'centimeters', 'PaperSize', [6 6]);
    hold on
    plot3(w_vec, real(FRF_vec), imag(FRF_vec), 'b-');
    plot3(w_vec, real(FRF_cf), imag(FRF_cf), 'r-');
    hold off
    
    view([-15, 30])
    
    xlabel('\omega')
    ylabel(sprintf('real H_{%d%d}', j, k))
    zlabel(sprintf('imag H_{%d%d}', j, k))
    
    print('-dpng','-r300', sprintf('%sCF_cmp_H%d%d3D', fig_dir, j, k))
end

function Y = ident_FRF(w, A_jk, w_res, eta_jk, Bdc)
    num_samples = length(w);
    num_gdl = length(w_res);
    
    Y = zeros(1, num_samples);
    for sample_i = 1:num_samples
        Y(sample_i) = Bdc;
        for gdl_i = 1:num_gdl
            Y(sample_i) = Y(sample_i) ...
                -A_jk(gdl_i)*w(sample_i)^2/(w_res(gdl_i)^2-w(sample_i)^2+1i*eta_jk(gdl_i)*w_res(gdl_i)^2);
        end
    end
end